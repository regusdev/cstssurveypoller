﻿using EmailOperations;
using Logging;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace GetAndProcessEmailsRunOnce
{
    public class Worker : IWorker
    {
        private ImapHelper _helper;
        private List<MyMessage> _emailList;
        private readonly Logger _logger;

        public Worker(Logger logger)
        {
            _logger = logger;
        }

        public void DoWork()
        {
            try
            {
                _helper = EmailOps.ConnectHelper();
            }
            catch (ImapException exception)
            {
                _logger.Log(string.Format("Can't Connect to Mailbox: " + exception), 0);
                Environment.Exit(0);
            }

            try
            {
                _emailList = EmailOps.GetMyMessages(_helper);
            }
            catch (ImapException exception)
            {
                _logger.Log(string.Format("Can't Get Messages: " + exception), 0);
                Environment.Exit(0);

            }

            _logger.Log("number of Emails: " + _emailList.Count, 1);
            var index = 0;

            foreach (var message in _emailList)
            {
                try
                {
                    _logger.Log("Message #" + index + " " + message.Message.Subject, 1);
                    index++;
                    CSTSDatabaseConnector.DatabaseConnector.SORCSConnector(message, _logger);

                    if (ConfigurationManager.AppSettings.Get("DeleteEmailOnRead") == "True")
                    {
                        EmailOps.DeleteMessage(_helper, message.UniqueId);
                    }
                }
                catch (IndexOutOfRangeException exception)
                {
                    _logger.Log("Error In create: " + exception, 0);
                }

            }
            _helper.Purge();

        }

    }

}
﻿using CSTSSurveyPoller.CSTSSurveyPoller;
namespace CSTSSurveyPoller
{
    public class TeamworksConnector
    {
        public static void UpdateInboundEmailQueueDetail(int emailQueueId)
        {
            ExternalAutomationPortTypeClient service = new ExternalAutomationPortTypeClient();
            service.FinaliseSurveyAsync(emailQueueId);

        }

    }
}
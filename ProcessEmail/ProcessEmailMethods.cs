﻿using Logging;
using System;
using System.Text;

namespace ProcessEmail
{
    public class ProcessEmailMethods
    {
        private static readonly StringBuilder notificationString = new StringBuilder();
        private readonly Logger _logger;

        public ProcessEmailMethods(Logger logger)
        {
            _logger = logger;
        }

        public PeopleSoftEmployeeDetails GetPeopleSoftEmployeeDetails(string text, DateTime receivedDateTime,
            string from, string subject)
        {

            var peopleSoftEmployeeDetails = new PeopleSoftEmployeeDetails
            {
                EmailFrom = @from,
                EmailSubject = subject
            };
            var lines = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var line in lines)
            {
                try
                {
                    var tokens = line.Trim().Split(':');
                    var propInfo =
                        peopleSoftEmployeeDetails.GetType()
                            .GetProperty(tokens[0].Replace(" ", "").Replace("-", "").Replace("(", "").Replace(")", ""));
                    if (propInfo != null)
                    {
                        if (propInfo.PropertyType.Name == "Int32")
                        {
                            propInfo.SetValue(peopleSoftEmployeeDetails, int.Parse(tokens[1].Trim()), null);
                        }
                        else
                            propInfo.SetValue(peopleSoftEmployeeDetails, tokens[1].Trim(), null);

                        notificationString.AppendLine(line);
                    }
                    else
                    {
                        _logger.Log("peopleSoftEmployeeDetails property not found: " + tokens[0] + " Please check Peoplesoft HR emails", 3);
                    }
                }
                catch (IndexOutOfRangeException)
                {

                }

            }
            return peopleSoftEmployeeDetails;

        }


    }
}
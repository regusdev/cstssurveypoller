﻿using TeamworksConnector.TeamworksConnector;

namespace TeamworksConnector
{
    public class CSTSConnector
    {
        public static void FinaliseSurveyInboundEmail(int inboundEmailQueueId)
        {
            ExternalAutomationPortTypeClient service = new ExternalAutomationPortTypeClient();
            service.FinaliseSurvey(inboundEmailQueueId);
        }
    }
}

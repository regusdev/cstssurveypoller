﻿using EmailOperations;
using Logging;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CSTSDatabaseConnector
{
    public class DatabaseConnector
    {
        public static void SORCSConnector(MyMessage messageDetails, Logger _logger)
        {
            // Create the connection to the resource
            using (SqlConnection conn = new SqlConnection())
            {
                // Create the connectionString
                // Trusted_Connection is used to denote the connection uses Windows Authentication
                conn.ConnectionString =
                    "Server=reg10vss02bpm.regus.local;Database=SORCS;User Id=bpm_sa;Password = bpm_sa";
                try
                {
                    conn.Open();

                    // Create the command
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[processor].[SurveyInsertEmail]";

                    SqlParameter inparm = cmd.Parameters.Add("@MSG_SUBJECT", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.Subject;
                    inparm = cmd.Parameters.Add("@MSG_TO", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.To.ToString();
                    inparm = cmd.Parameters.Add("@MSG_FROM", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.From.ToString();
                    inparm = cmd.Parameters.Add("@MSG_DATE", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.Date.ToString();
                    inparm = cmd.Parameters.Add("@MSG_CC", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.CC.ToString();
                    inparm = cmd.Parameters.Add("@MSG_REPLY", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.ReplyTo.ToString();
                    inparm = cmd.Parameters.Add("@MSG_NARRATIVE", SqlDbType.NVarChar);
                    inparm.Direction = ParameterDirection.Input;
                    inparm.Value = messageDetails.Message.BodyText;

                    SqlParameter ouparm = cmd.Parameters.Add("@InboundEmailProcessQueueID", SqlDbType.Int);
                    ouparm.Direction = ParameterDirection.Output;

                    SqlParameter retval = cmd.Parameters.Add("return_value", SqlDbType.Int);
                    retval.Direction = ParameterDirection.ReturnValue;

                    _logger.Log("Writing message: " + messageDetails.Message.Subject, 0);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();

                    Console.WriteLine("The output parameter value is {0}",
                        cmd.Parameters["@InboundEmailProcessQueueID"].Value);

                    TeamworksConnector.CSTSConnector.FinaliseSurveyInboundEmail((int)cmd.Parameters["@InboundEmailProcessQueueID"].Value);

                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
